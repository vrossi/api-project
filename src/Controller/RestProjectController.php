<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\Projet;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * @Route("/rest/project", name="rest_project")
 */
class RestProjectController extends Controller
{
    private $serializer;
    
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index() {
        
        $repo = $this->getDoctrine()->getRepository(Projet::class);
       
        $json = $this->serializer->serialize($repo->findAll(), "json");
        
        return JsonResponse::fromJsonString($json);
    }
    
    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request, ObjectManager $manager) {
      

        
        $content = json_decode($request->getContent(), true);

        $projet = new Projet();

        $projet->setName($content["name"]);
        $projet->setDescription($content["description"]);
        $projet->setBase64Image($content["image"]);
        $projet->setLinkButton($content["link"]);

        $manager->persist($projet);
        $manager->flush();
        return new Response();

    }

    /**
     * @Route("/{projet}", methods={"GET"})
     */
    public function single(Projet $projet) {
        $json = $this->serializer->serialize($projet,"json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{projet}", methods={"DELETE"})
     */
    public function remove(Projet $projet) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($projet);
        $manager->flush();
        return new Response("", 204);
    }

    /**
     * @Route("/{projet}", methods={"PUT"})
     */
    public function update(Projet $projet, ObjectManager $manager,  Request $request) {

        $content = json_decode($request->getContent(), true);

        $projet->setName($content["name"]);
        
        $projet->setDescription($content["description"]);
        if($content["image"] != null) {
            $projet->setBase64Image($content["image"]);
        }
        $projet->setLinkButton($content["link"]);
        $manager->persist($projet);
        $manager->flush();
        return new Response();

    }

    
}
