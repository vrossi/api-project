<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 */
class Projet
{

    const IMAGES_PATH = 'images/';
    const MIME_TYPES = ['image/jpeg', 'image/png'];



    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $linkButton;


    public function __construct(String $base64Image = "")
    {
        
        $this->image = "";
        if ($base64Image !== "")
        {
            $this->setBase64Image($base64Image);
        }
    }


    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }



    public function setBase64Image(String $base64Image)
    {
        
        if(is_file(__DIR__ . "/../../public/" . $base64Image)){
            $this->image = $base64Image;
            return $this;
        }
        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode(',', $base64Image);
        // we could add validation here with ensuring count( $data ) > 1
        $decodedImage = base64_decode($data[1]);
        // open the output file for writing
        $file = fopen('/tmp/tempImage', 'wb');
        fwrite($file, $decodedImage);
        // clean up the file resource
        fclose($file);
        $imageType = mime_content_type('/tmp/tempImage');
        // if mime type is allowed
        if (in_array($imageType, self::MIME_TYPES)){
            // if an image is already attached to the product ...
            if ($this->image !== "") {
                // ... delete it
                unlink($this->image);
            }
            // generate a unique filename
            $filename = md5(uniqid());
            // get extension e.g png
            preg_match('/.*\/(.*)/', $imageType, $matches);
            $extension = $matches[1];
            // construct file path + name + extension string
            $filePath = self::IMAGES_PATH . $filename . '.' . $extension;
            // move from template to our storage folder
            rename('/tmp/tempImage', $filePath);
            // save image
            $this->image = $filePath;
        }
        return $this;
    }

    
    public function getLinkButton(): ?string
    {
        return $this->linkButton;
    }

    public function setLinkButton(string $linkButton): self
    {
        $this->linkButton = $linkButton;

        return $this;
    }
}
